import discord
import asyncio
import logging
import aiohttp
import xivapi

TOKEN = ''

client = discord.Client()
xiv_client = None
session = None

async def closeSession():
    global session
    if session != None:
        print('closing session')
        await session.close()

async def getXivClient():
    global xiv_client
    global session
    if xiv_client == None:
        loop = asyncio.get_event_loop()
        if session ==None:
            session = aiohttp.ClientSession(loop=loop)
        print('new INstance')

        xiv_client = xivapi.Client(session=session, api_key='')

        return xiv_client

async def fetch_datacenter_market(id: int, datacenter, history=25):
    market = await xiv_client.market_by_datacenter(
            item_id=id,
            datacenter=datacenter,
            max_history=history
            )
    return market

def lowest_price(server, server_name):
    result = {'lowestPrice':None, 'id':None, 'server_name':server_name}
    for key, value in server.items():
        if key == 'Server':
            result['id'] = value
        if key == 'Prices':
            for element in value:
                if result['lowestPrice'] == None:
                    result['lowestPrice'] = element['PricePerUnit']
                if element['PricePerUnit'] < result['lowestPrice']:
                    result['lowestPrice'] = element['PricePerUnit']
    return result

async def lowest_price_on_servers(datacenter, item_id):
    market = await fetch_datacenter_market(item_id, datacenter)
    lowestPrice = None
    for server in market:
        answer = lowest_price(market.get(server), server)
        if lowestPrice == None:
            lowestPrice = answer
        elif lowestPrice.get('lowestPrice') > answer.get('lowestPrice'):
            lowestPrice = answer
    return lowestPrice

@client.event
async def on_message(message):
    if message.content.startswith('!lowestprices'):
        command = message.content.split()
        datacenter = command[1]
        item_id = command[2]
        await getXivClient()

        result = await lowest_price_on_servers(datacenter,item_id)
        msg = 'The lowest price at this datacenter is: {0} and it can be found on server: {1}'.format(result.get('lowestPrice'), result.get('server_name'))
        await closeSession()
        await client.send_message(message.channel, msg)

@client.event
async def on_ready():
    print('Logged In')

client.run(TOKEN)
