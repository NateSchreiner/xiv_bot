# xiv_bot

<h4> This is a simple discord bot writen in python used for Final Fantasy XIV players.
 While script is running you can ask the bot via </h4>
    
    !lowestprices "DataCenter" "Item_id"

<h4> what the lowest price is for the item given by  "item_id" in the datacenter referenced by  "datacenter"
 This command will tell you the lowest price and what server you can find it on</h4>


<h4> This bot has the potential to be developed much more thouroughly with many more commands</h4>

<h4> To get started using or developing</h4>

    pip install discord.py
    pip install asyncio
    pip install aiohttp
    pip install xivapi


<h4>Aside from dependancies you will need a discord server and a bot authorized for that server. Once that is complet you will need to get the bots token and input that into the python file. Also you will need an API_KEY from xivapi which will also need to be inputed into the script.

<h4>Once dependancies are installed and keys are retreived and inputed, run the bot via:</h4>

    python3 xiv_discord_bot.py


